FROM justinribeiro/chrome-headless

# default values for environment variables
ARG URL
ENV LIGHTHOUSE_SCORE_THRESHOLD=0.80

USER root

RUN apt-get update
RUN apt-get install -y bc curl gnupg2 sudo
RUN curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
RUN apt-get install -y nodejs
RUN npm config set registry http://registry.npmjs.org/
RUN npm install -g lighthouse

ADD ./run.sh /tmp/run.sh

ENTRYPOINT /bin/bash /tmp/run.sh
